const Joi = require('joi');

const registerValidation = (data) => {
  const schema = Joi.object({
    email: Joi.string().min(6).required().email(),
    password: Joi.string().min(6).required().pattern(
        new RegExp('^[a-zA-Z0-9]{3,30}$')),
    role: Joi.string().valid('SHIPPER', 'DRIVER').required(),
  });
  return schema.validate(data);
};

const loginValidation = (data) => {
  const schema = Joi.object({
    email: Joi.string().min(6).required().email(),
    password: Joi.string().min(6).required().pattern(
        new RegExp('^[a-zA-Z0-9]{3,30}$')),
  });
  return schema.validate(data);
};

const resetPasswordValidation = (data) => {
  const schema = Joi.object({
    oldPassword: Joi.string().min(6).required(),
    newPassword: Joi.string().min(6).required().pattern(
        new RegExp('^[a-zA-Z0-9]{3,30}$')),
  });
  return schema.validate(data);
};

const truckValidation = (data) => {
  const schema = Joi.object({
    type: Joi.string().valid('SPRINTER', 'SMALL STRAIGHT', 'LARGE STRAIGHT'),
  });
  return schema.validate(data);
};


module.exports.registerValidation = registerValidation;
module.exports.loginValidation = loginValidation;
module.exports.resetPasswordValidation = resetPasswordValidation;
module.exports.truckValidation = truckValidation;

