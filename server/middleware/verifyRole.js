const jwt = require('jsonwebtoken');

module.exports = function(roles) {
  return function(req, res, next) {
    const token = req.headers.authorization?.split(' ')[1];
    if (!token) {
      return res.status(400).json({message: 'Access denied'});
    }
    try {
      const verified = jwt.verify(token, process.env.TOKEN_SECRET);
      req.user = verified;
      let hasRole = false;
      if (req.user.role === roles) {
        hasRole = true;
      }
      if (!hasRole) {
        return res.status(400).json(
            {message: 'Role access denied'});
      }
      next();
    } catch (err) {
      res.status(400).json({message: 'Invalid Token'});
    }
  };
};
