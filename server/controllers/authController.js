const User = require('../models/user');
const jwt = require('jsonwebtoken');
const bcrypt = require('bcryptjs');
const {registerValidation, loginValidation} = require(
    '../middleware/validation');

exports.register = async (req, res) => {
  const {error} = registerValidation(req.body);
  if (error) return res.status(400).json({message: error.details[0].message});

  try {
    const emailExist = await User.findOne({email: req.body.email}).exec();
    if (emailExist) {
      return res.status(400).json({message: 'Email already exists'});
    }

    const salt = await bcrypt.genSalt(10);
    const hashedPassword = await bcrypt.hash(req.body.password, salt);
    const user = new User({
      email: req.body.email,
      password: hashedPassword,
      role: req.body.role,
    });
    await user.save();
    res.status(200).json({message: 'Success'});
  } catch (err) {
    res.status(500).json({message: err.message});
  }
};

exports.login = async (req, res) => {
  try {
    const user = await User?.findOne(
        {email: req.body.email}).exec();
    const {error} = loginValidation(req.body);
    if (error) return res.status(400).json({message: error.details[0].message});

    if (!user) {
      return res.status(400).json({message: 'Email does not exist'});
    }
    const validPass = await bcrypt.compare(req.body.password, user.password);
    if (!validPass) {
      return res.status(400).json({message: 'Invalid password'});
    }
    const token = jwt.sign(
        {_id: user._id, role: user.role},
        process.env.TOKEN_SECRET);
    res.status(200).json({message: 'Success',
      jwt_token: token,
    });
  } catch (err) {
    res.status(500).json({message: err.message});
  }
};


