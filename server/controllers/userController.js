const User = require('../models/user');
const bcrypt = require('bcryptjs');
const {resetPasswordValidation} = require(
    '../middleware/validation');


exports.getUser = async (req, res) => {
  const me = await User.findOne({_id: req.user._id});
  try {
    res.status(200).json({user: {
      _id: me._id,
      role: me.role,
      email: me.email,
      createdDate: me.createdDate}});
  } catch (err) {
    res.status(500).json({message: err.message});
  }
};

exports.changePassword = async (req, res) => {
  const {error} = resetPasswordValidation(req.body);
  if (error) return res.status(400).json({message: error.details[0].message});

  const user = await User.findById(req.user._id);

  const validPass = await bcrypt.compare(req.body.oldPassword, user.password);
  if (!validPass) {
    return res.status(400).json({message: 'Invalid password'});
  }
  try {
    const salt = await bcrypt.genSalt(10);
    const hashedPassword = await bcrypt.hash(req.body.newPassword, salt);
    await User.updateOne({_id: req.user._id},
        {
          $set: {password: hashedPassword},
        });
    res.status(200).json({message: 'Password changed successfully'});
  } catch (err) {
    res.status(500).json({message: err.message});
  }
};

exports.deleteUser = async (req, res) => {
  try {
    await User.remove({_id: req.user._id});
    res.status(200).json({message: 'Profile deleted successfully'});
  } catch (err) {
    res.status(500).json({message: err.message});
  }
};
