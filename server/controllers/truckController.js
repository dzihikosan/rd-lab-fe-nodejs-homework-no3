const Truck = require('../models/truck');
const {truckValidation} = require(
    '../middleware/validation');


exports.getTruck = async (req, res) => {
  const trucks = await Truck.find({created_by: req.user._id});
  try {
    res.status(200).json({trucks: trucks});
  } catch (err) {
    res.status(500).json({message: err.message});
  }
};


exports.addTruck = async (req, res) => {
  const {error} = truckValidation(req.body);
  if (error) return res.status(400).json({message: error.details[0].message});
  try {
    const truck = new Truck({
      created_by: req.user._id,
      type: req.body.type || null,
    });
    await truck.save();
    res.status(200).json({message: 'Truck created successfully'});
  } catch (err) {
    res.status(500).json({message: err.message});
  }
};

exports.updateTruckId = async (req, res) => {
  try {
    const {error} = truckValidation(req.body);
    if (error) return res.status(400).json({message: error.details[0].message});
    const truck = await Truck.findById(req.params.id);
    if (truck.assigned_to === req.user._id) {
      return res.status(400).json(
          {message: 'You can not update this truck'});
    };
    await Truck.findByIdAndUpdate(req.params.id, req.body);
    res.status(200).json({message: 'Truck details changed successfully'});
  } catch (err) {
    res.status(500).json({message: err.message});
  }
};

exports.getTruckId = async (req, res) => {
  try {
    const truck = await Truck.findById(req.params.id);
    res.status(200).json({truck: truck});
  } catch (err) {
    res.status(500).json({message: err.message});
  }
};

exports.deleteTruckId = async (req, res) => {
  try {
    const truck = await Truck.findById(req.params.id);
    if (truck.assigned_to === req.user._id) {
      return res.status(400).json(
          {message: 'You can not delete this truck'});
    };
    await Truck.remove({_id: req.params.id});
    res.status(200).json({message: 'Truck deleted successfully'});
  } catch (err) {
    res.status(500).json({message: err.message});
  }
};

exports.assignTruckId = async (req, res) => {
  try {
    const assignedTruck = await Truck.find({assigned_to: req.user._id});
    if (assignedTruck.length>0) {
      return res.status(400).json(
          {message: 'You can not assign more then one truck'});
    }
    const truck = await Truck.findById(req.params.id);
    if (truck.status !== 'IS') {
      return res.status(400).json(
          {message: 'You can not assign this truck'});
    };
    await Truck.updateOne({_id: req.params.id},
        {
          $set: {assigned_to: req.user._id},
        });
    res.status(200).json({message: 'Truck assigned successfully'});
  } catch (err) {
    res.status(500).json({message: err.message});
  }
};


