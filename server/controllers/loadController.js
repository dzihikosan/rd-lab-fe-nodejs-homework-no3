const Load = require('../models/load');
const Truck = require('../models/truck');


exports.getLoads = async (req, res) => {
  const status = req.query.status || ' ';
  const limit = +req.query.limit || 10;
  if (limit>50) {
    return res.status(400).json(
        {message: 'Limit must be max 50'});
  }
  const offset = +req.query.offset || 0;
  if (req.user.role === 'SHIPPER') {
    try {
      if (status === ' ') {
        const loads = await Load.find(
            {created_by: req.user._id}).skip(offset).limit(limit);
        res.status(200).json({loads: loads});
      } else {
        const loads = await Load.find(
            {created_by: req.user._id}).find(
            {status: status}).skip(offset).limit(limit);
        res.status(200).json({loads: loads});
      }
    } catch (err) {
      res.status(500).json({message: err.message});
    }
  } else if (req.user.role === 'DRIVER') {
    try {
      if (status === ' ') {
        const loads = await Load.find({assigned_to: req.user._id}).find(
            {status: {$in: ['SHIPPED', 'ASSIGNED']}}).skip(offset).limit(limit);
        res.status(200).json({loads: loads});
      } else {
        const loads = await Load.find({assigned_to: req.user._id}).find(
            {status: {$in: ['SHIPPED', 'ASSIGNED']}}).find(
            {status: status}).skip(offset).limit(limit);
        res.status(200).json({loads: loads});
      }
    } catch (err) {
      res.status(500).json({message: err.message});
    }
  }
};


exports.addLoad = async (req, res) => {
  try {
    const load = new Load({
      created_by: req.user._id,
      name: req.body.name || '',
      payload: req.body. payload || '',
      pickup_address: req.body.pickup_address || '',
      delivery_address: req.body.delivery_address || '',
      dimensions: req.body.dimensions || '',
    });
    await load.save();
    res.status(200).json({message: 'Load created successfully'});
  } catch (err) {
    res.status(500).json({message: err.message});
  }
};

exports.getActiveLoad = async (req, res) => {
  try {
    const load = await Load.findOne({assigned_to: req.user._id}).find(
        {status: 'ASSIGNED'});
    res.status(200).json({load: load[0]});
  } catch (err) {
    res.status(500).json({message: err.message});
  }
};

exports.getLoadId = async (req, res) => {
  try {
    const load = await Load.findById(req.params.id);
    res.status(200).json({load: load});
  } catch (err) {
    res.status(500).json({message: err.message});
  }
};

exports.updateLoadId = async (req, res) => {
  try {
    const load = await Load.findById(req.params.id);
    if (load.status !== 'NEW') {
      return res.status(400).json(
          {message: 'You can not update this load'});
    };
    await Load.findByIdAndUpdate(req.params.id, req.body);
    res.status(200).json({message: 'Load details changed successfully'});
  } catch (err) {
    res.status(500).json({message: err.message});
  }
};


exports.deleteLoadId = async (req, res) => {
  try {
    const load = await Load.findById(req.params.id);
    if (load.status !== 'NEW') {
      return res.status(400).json(
          {message: 'You can not delete this load'});
    };
    await Load.remove({_id: req.params.id});
    res.status(200).json({message: 'Load deleted successfully'});
  } catch (err) {
    res.status(500).json({message: err.message});
  }
};


exports.getShippingId = async (req, res) => {
  try {
    const load = await Load.findById(req.params.id);
    const truck = await Truck.findOne({assigned_to: load.assigned_to}).find(
        {status: 'OL'});
    res.status(200).json({load: load,
      truck: truck[0]});
  } catch (err) {
    res.status(500).json({message: err.message});
  }
};


exports.iterateState = async (req, res) => {
  try {
    const loads = await Load.find({assigned_to: req.user._id,
      status: 'ASSIGNED',
    });
    if (loads.length == 0) {
      return res.status(400).json({message: 'You have no active load'});
    }
    const state = loads[0].state;
    let switchState;
    switch (state) {
      case 'En route to Pick Up':
        switchState ='Arrived to Pick Up';
        break;
      case 'Arrived to Pick Up':
        switchState = 'En route to delivery';
        break;
      case 'En route to delivery':
        switchState = 'Arrived to delivery';
        break;
    }
    if (state === 'Arrived to delivery') {
      return res.status(400).json(
          {message: 'You can not update the state anymore'});
    }
    await Load.updateOne({assigned_to: req.user._id,
      status: 'ASSIGNED',
    }, {
      $set: {state: switchState},
    });
    loads[0].logs.push(
        {message: `Load state changed to '${switchState}'`,
          time: new Date()});
    await Load.updateOne({assigned_to: req.user._id,
      status: 'ASSIGNED',
    }, {
      $set: {logs: loads[0].logs},
    });
    if (state === 'En route to delivery') {
      await Load.updateOne({assigned_to: req.user._id,
        status: 'ASSIGNED',
      }, {
        $set: {status: 'SHIPPED'},
      });
      await Truck.updateOne({assigned_to: req.user._id, status: 'OL'}, {
        $set: {status: 'IS'},
      });
    }
    res.status(200).json(
        {message: `Load state changed to '${switchState}'`});
  } catch (err) {
    res.status(500).json({message: err.message});
  }
};


exports.postLoadId = async (req, res) => {
  try {
    const load = await Load.findById(req.params.id);
    await Load.updateOne({_id: req.params.id}, {
      $set: {status: 'POSTED'},
    });
    load.logs.push(
        {message: 'Load status changed to "POSTED"',
          time: new Date()});
    await Load.updateOne({_id: req.params.id}, {
      $set: {logs: load.logs},
    });
    let sizes;
    if (load.payload < 4000 && load.dimensions.width< 350 &&
      load.dimensions.height < 200 && load.dimensions.length < 700) {
      sizes = {$in: ['LARGE STRAIGHT', 'SMALL STRAIGHT', 'SPRINTER']};
    }
    if (load.payload > 2500 && load.dimensions.width> 250 &&
      load.dimensions.height > 170 && load.dimensions.length > 500) {
      sizes = 'LARGE STRAIGHT';
    }
    if (load.payload < 2500 && load.dimensions.width< 250 &&
      load.dimensions.height < 170 && load.dimensions.length < 500) {
      sizes = {$in: ['SMALL STRAIGHT', 'SPRINTER']};
    }
    if (load.payload < 1700 && load.dimensions.width< 250 &&
      load.dimensions.height < 170 && load.dimensions.length < 300) {
      sizes = 'SPRINTER';
    }


    const truck = await Truck.find({status: 'IS', type: sizes,
    });
    const validTruck = truck.filter((value)=>{
      return value.assigned_to === value.created_by;
    });
    if (validTruck.length === 0) {
      await Load.updateOne({_id: req.params.id}, {
        $set: {status: 'NEW'},
      });
      load.logs.push(
          {message: 'Load status changed to "NEW"',
            time: new Date()});
      await Load.updateOne({_id: req.params.id}, {
        $set: {logs: load.logs},
      });
      return res.status(200).json({
        message: 'Load posted successfully',
        driver_found: false,
        sizes: sizes,
      });
    } else {
      await Load.updateOne({_id: req.params.id}, {
        $set: {assigned_to: validTruck[0].assigned_to},
      });
      await Load.updateOne({_id: req.params.id}, {
        $set: {status: 'ASSIGNED'},
      });
      await Load.updateOne({_id: req.params.id}, {
        $set: {state: 'En route to Pick Up'},
      });
      load.logs.push(
          {message: 'Load status changed to "ASSIGNED"',
            time: new Date()});
      load.logs.push(
          {message: `Load assigned to driver with id 
          ${validTruck[0].assigned_to}`,
          time: new Date()});
      load.logs.push(
          {message: 'Load state changed to "En route to Pick Up"',
            time: new Date()});
      await Load.updateOne({_id: req.params.id}, {
        $set: {logs: load.logs},
      });
      await Truck.updateOne({assigned_to: validTruck[0].assigned_to}, {
        $set: {status: 'OL'},
      });
      res.status(200).json({
        message: 'Load posted successfully',
        driver_found: true,
      });
    }
  } catch (err) {
    res.status(500).json({message: err.message});
  }
};

