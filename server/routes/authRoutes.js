const express =require('express');

const route = new express.Router();


const controller = require('../controllers/authController');

route.post('/register', controller.register);
route.post('/login', controller.login);

module.exports=route;
