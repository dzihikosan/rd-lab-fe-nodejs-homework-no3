const express =require('express');
const verifyRole = require('../middleware/verifyRole');
const route = new express.Router();


const controller = require('../controllers/truckController');

route.get('/', verifyRole('DRIVER'), controller.getTruck);
route.post('/', verifyRole('DRIVER'), controller.addTruck);
route.get('/:id', verifyRole('DRIVER'), controller.getTruckId);
route.put('/:id', verifyRole('DRIVER'), controller.updateTruckId);
route.delete('/:id', verifyRole('DRIVER'), controller.deleteTruckId);
route.post('/:id/assign', verifyRole('DRIVER'), controller.assignTruckId);

module.exports=route;
