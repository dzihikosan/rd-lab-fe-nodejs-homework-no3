const express = require('express');
const route = new express.Router();
const verifyRole = require('../middleware/verifyRole');
const verify = require('../middleware/verifyToken');

const controller = require('../controllers/loadController');

route.get('/', verify, controller.getLoads);
route.post('/', verifyRole('SHIPPER'), controller.addLoad);
route.get('/active', verifyRole('DRIVER'), controller.getActiveLoad);
route.patch('/active/state', verifyRole('DRIVER'), controller.iterateState);
route.get('/:id', verify, controller.getLoadId);
route.put('/:id', verifyRole('SHIPPER'), controller.updateLoadId);
route.delete('/:id', verifyRole('SHIPPER'), controller.deleteLoadId);
route.post('/:id/post', verifyRole('SHIPPER'), controller.postLoadId);
route.get('/:id/shipping_info',
    verifyRole('SHIPPER'), controller.getShippingId);


module.exports=route;
