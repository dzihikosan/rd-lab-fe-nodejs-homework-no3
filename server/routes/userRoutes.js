const express =require('express');
const verifyToken = require('../middleware/verifyToken');
const route = new express.Router();


const controller = require('../controllers/userController');

route.get('/', verifyToken, controller.getUser);
route.delete('/', verifyToken, controller.deleteUser);
route.patch('/password', verifyToken, controller.changePassword);


module.exports=route;
