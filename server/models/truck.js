const mongoose = require('mongoose');

const truckSchema = new mongoose.Schema({
  created_by: {
    type: String,
  },
  assigned_to: {
    type: String,
    default: ' ',
  },
  type: {
    type: String,
  },
  status: {
    type: String,
    enum: ['OL', 'IS'],
    default: 'IS',
  },
  created_date: {
    type: Date,
    default: Date.now,
  },
}, {
  versionKey: false,
});

module.exports = mongoose.model('Truck', truckSchema);
