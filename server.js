const express = require('express');
const dotenv = require('dotenv');
const morgan = require('morgan');
const mongoose = require('mongoose');
const app = express();

dotenv.config({path: './server/config/config.env'});


mongoose.connect(process.env.Mongo_URL, {useNewUrlParser: true},
    () => console.log('MongoDB connected'));


app.use(express.json());

const PORT = process.env.PORT;

app.use(morgan('common'));


app.use('/api/auth', require('./server/routes/authRoutes'));
app.use('/api/loads', require('./server/routes/loadRoutes'));
app.use('/api/trucks', require('./server/routes/truckRoutes'));
app.use('/api/users/me', require('./server/routes/userRoutes'));


app.listen(PORT, () => {
  console.log(`Server is running on http://localhost:${PORT}`);
});

